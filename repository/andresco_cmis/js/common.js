/* 
 * Function which has to be used everywhere
 */
function request(config) {
    var data = {};

    if (config.form) {
        if (typeof(config.form) == 'string') {
            config.form = $("#" + config.form)[0];
        }
        if (!config.url && config.form.action) {
            config.url = config.form.action;
        }

        if (config.form.method && !config.method) {
            config.method = config.form.method;
        }

        for (var i = 0; i < config.form.elements.length; i++) {
            var element = config.form.elements[i];
            if (!element.name) {
                continue;
            }

            //Select type multiple
            if (element.multiple) {
                var opt = element.options;
                var values = [];
                for (var j = 0; j < opt.length; j++) {
                    if (opt[j].selected)
                        values.push(opt[j].value);
                }
                var optval = values.join('|');
                data[element.name] = optval;
            }
            else if (element.type == 'checkbox') {
                if (element.checked) {
                    data[element.name] = element.value;
                }
                else {
                    data[element.name] = false;
                }
            }
            else if (element.type == 'radio') {
                if (element.checked) {
                    data[element.name] = element.value;
                }
            }
            else if (element.name) {
                data[element.name] = element.value;
            }
        }
    }

    for (var key in config.data) {
        data[key] = config.data[key];
    }

    if (!config.method) {
        config.method = "POST";
    }

    $.ajax({
        async: (typeof(config.async) != 'undefined') ? config.async : true,
        type: config.method,
        url: config.url,
        data: data,
        success: function(response, code) {
            if (config.container) {
                config.container.innerHTML = response;
            }

            if (config.success) {
                config.success(response);
            }

            //Parsing response javascript
            var jscode = "";
            var parts = response.match(/<script[^>]*>(.|\n|\t|\r)*?<\/script>/gi);
            if (parts) {
                for (i = 0; i < parts.length; i++) {
                    jscode += parts[i].replace(/<script[^>]*>|<\/script>/gi, "");
                    response = response.replace(parts[i], "");
                }
            }


            if (jscode != "") {
                var incomingScript = document.createElement('SCRIPT');
                incomingScript.type = 'text/javascript';
                incomingScript.lang = 'javascript';
                incomingScript.defer = true;
                incomingScript.text = jscode;
                document.getElementsByTagName('head')[0].appendChild(incomingScript);
            }

        },
        error: function() {
        }
    });
}

jQuery(document).ready(function(){
	$("#id_entity_type").on('change', '', function(){
		var postdata = {'task' : 'getentity', 'entity_type' : $(this).val()};
		$.ajax({
			  type: "POST",
			  url: "./ajaxcallback.php",
			  data: postdata,
			  success: function(response){
				  $("#id_entity_id").html('');
				  $("#id_entity_id").append($('<option>', {
					    value: '-1',
					    text: 'Select'
					}));
				  for(i=0; i < response.length; i++) {
					  $("#id_entity_id").append($('<option>', {
						    value: response[i].id,
						    text: response[i].name
						}));
				  }
			  },
			  dataType : 'json'
		});
	});
});


