<?php

/**
 *-----------------------------------------------------------------------------
 * Andresco Repository Plugin
 * User Library (Same-Signon Functionality)
 *
 * @copyright 2014+ Androgogic Pty Ltd
 * @link      http://www.androgogic.com.au/andresco
 *
 *-----------------------------------------------------------------------------
 **/

require_once($CFG->dirroot . "/repository/andresco_cmis/util/andrescologger.class.php");

class user_service extends RestClient {

    private $_log;
    private $repositoryid;

    public function __construct($repositoryid, $base_url, $username, $password, $options) {
        
        global $SESSION, $CFG, $USER;
        parent::__construct($username, $password);
        
        $this->_log = new AndrescoLogger('userlib.php');

        $this->_log->info('__construct', "Repository ID=$repositoryid");
        $this->_log->debug('__construct', "Base URL=$base_url");
        $this->_log->debug('__construct', "Username=$username");
        $this->_log->debug('__construct', "Password=$password");
        $this->_log->debug('__construct', "Options=" . print_r($options, true));

        // These should probably be declared as private variables and use _format e.g. _repositoryid
        $this->repositoryid = $repositoryid;
        $this->base_url = $base_url;
        $this->peopleUrl = $this->base_url . '/alfresco/service/api/people';
        $this->username = $username;
        $this->password = $password;
        $this->sessname = 'alfresco_ticket'; // HACK
        $andresco_options = $options;

        if (!empty($SESSION->{$this->sessname}[$repositoryid])) {
            $this->ticket = $SESSION->{$this->sessname}[$repositoryid];            
            $this->auth_options = array('alf_ticket' => $this->ticket);
            $this->_log->info('__construct', "Using existing ticket in session=$this->ticket");
        }

    }

    /**
     *------------------------------------------------------------------------------
     * Generate an Alfresco ticket for the Andresco connection admin user. This is 
     * refered to as the admin ticket.
     *------------------------------------------------------------------------------
     * @param   None
     * @return  Alfresco admin ticket if successful
     *          FALSE if unable to get an admin ticket
     *------------------------------------------------------------------------------
     * @todo    Coding convention should stick to moodle style: run_as_admin_user()
     *          but there are calls to this function in lib.php to be mindful of.
     *          Further error handling of doPost result (e.g. result->code == 200)
     *------------------------------------------------------------------------------    
     **/    

    public function run_as_admin() {

        $pwd = trim($this->password);
        $u = trim($this->username);
        $api_url = $this->base_url . "/alfresco/service/api/login";
        $data = json_encode(array("username" => $u, "password" => $pwd));
        $result = $this->doPost($api_url, $data, 'application/json');        
        
        if ($result != '') { // Probably should be checking the result->code == 200 here??
            $response = json_decode(trim($result->body));
            $admin_ticket = $response->data->ticket;
            $this->_log->info('run_as_admin', "Successfully retrieved admin ticket=$admin_ticket");
            return $admin_ticket;
        } else {
            // This is not good, Andresco can't do much if it can't get a ticket for the
            // connection user.
            $this->_log->error('run_as_admin', "Unable to get an admin ticket=$ticket");
            throw new Exception(get_string('unabletogetadminticket', 'repository_alfresco'));
            return FALSE;
        }

    }

    /**
     *------------------------------------------------------------------------------
     * Generate an Alfresco ticket for the moodle user. This is referred to as the
     * "user" ticket.
     *------------------------------------------------------------------------------
     * @param   Alfresco Username
     * @param   Admin ticket required to perform runAs
     * @return  Alfresco ticket if successful, FALSE if unsuccessful
     *------------------------------------------------------------------------------
     **/    

    public function run_as_user($alfresco_username = null, $admin_ticket) {
        global $USER;

        $this->_log->info('run_as_user', "Alfresco Username=$alfresco_username");
        $this->_log->info('run_as_user', "Admin Ticket=$admin_ticket");

        if ($admin_ticket != '' && $admin_ticket != '0') {
            $this->auth_options = array('alf_ticket' => $admin_ticket);
            $alfresco_username_string = 'user=' . $alfresco_username;
            $url = $this->base_url . "/alfresco/service/andresco/alfresco/runas?" . $alfresco_username_string;
            $this->_log->debug('run_as_user', "RunAs URL=$url");
            $result_ticket = $this->doRequest($url);
            $result_ticket_decoded = json_decode($result_ticket->body);
            $alf_ticket = $result_ticket_decoded->alf_ticket;
            
            if (!isset($result_ticket_decoded) || $result_ticket->code == 404) {
                $this->_log->error('run_as_user', "Unable to get a user ticket, using admin ticket");
                return $admin_ticket;
            }
            if ($alf_ticket != '') {
                $pos = strpos($alf_ticket, 'TICKET');
                if ($pos === false) {
                    $this->_log->error('run_as_user', "Invalid ticket returned");
                    return FALSE;
                } else {
                    $this->auth_options = array('alf_ticket' => $alf_ticket);
                    $validate_ticket = $this->validate_alfresco_ticket($alf_ticket);
                    return $validate_ticket;
                }
            } else {
                $this->_log->error('run_as_user', "Empty ticket returned");
                return FALSE;
            }
        }
    }

    /**
     *------------------------------------------------------------------------------
     * Authenticate the user against Alfresco. If the user doesn't exist, first
     * create them in Alfresco.
     *------------------------------------------------------------------------------
     * @param   Admin Alfresco Ticket
     * @param   User's Alfresco Group (based on their Moodle Role)
     * @return  Alfresco user ticket
     *------------------------------------------------------------------------------
     **/    

    public function authenticate_alfresco_user($admin_ticket, $group_name) {
        global $USER;

        $user_ticket = 0;
        $alfresco_username = $this->get_unique_alfresco_username();
        $user_ticket = $this->run_as_user($alfresco_username, $admin_ticket);

        // if there is no user_ticket obtained or it simply returned the admin ticket, 
        // then runAs did not complete successfully as the user.
        if ($user_ticket == FALSE || $user_ticket==$admin_ticket) { 

            // Does the user exist?
            $user_existence_result = $this->user_exists_in_alfresco($admin_ticket, $alfresco_username);
            if ($user_existence_result == true) {
                $this->_log->info('authenticate_alfresco_user', "User exists in Alfresco=$alfresco_username");
                // Get a user ticket for the user
                $user_ticket = $this->run_as_user($alfresco_username, $admin_ticket);                                
            } else {
                // No, create a new user using the unique alfresco username
                $this->_log->info('authenticate_alfresco_user', "Creating new user=$alfresco_username");
                $create_user_result = $this->create_user($admin_ticket, $group_name);
                if ($create_user_result->userName != '') {
                    $this->_log->info('authenticate_alfresco_user', "New user created successfully");
                    $user_ticket = $this->run_as_user($create_user_result->userName, $admin_ticket);
                }
                else {
                    // Something went wrong with user creation. Fail gracefully to using admin ticket
                    $this->_log->error('authenticate_alfresco_user', "Failed to create user=$alfresco_username");
                }
            }
            if ($user_ticket == FALSE || $user_ticket==$admin_ticket) {
                $this->_log->warn('authenticate_alfresco_user', "Unable to get user ticket, using admin ticket");
                $user_ticket = $admin_ticket;
            }            
        }

        if (!empty($user_ticket)) {
            if ($admin_ticket) {
                $this->auth_options = array('user_ticket' => $admin_ticket);
                //assign the existing user to alfresco repository group  
                if (!empty($group_name)) {
                    $url = $this->base_url . "/alfresco/service/api/groups/" . rawurlencode($group_name) . "/children/" . $alfresco_username;
                    $contentType = 'application/json';
                    $this->doRequest($url, 'POST', '{}', $contentType);
                }
            }

            $this->auth_options = array('user_ticket' => $user_ticket);
        }

        return $user_ticket;
    }

    /**
     *------------------------------------------------------------------------------
     * Validate the Alfresco ticket provided.
     *------------------------------------------------------------------------------
     * @param   Alfresco Ticket
     * @return  Validated ticket if successful
     *          FALSE if unable to validate ticket
     *------------------------------------------------------------------------------
     * @todo    This probably only works for connection user as validation
     *          must be by the user that the ticket is for. That a user cannot
     *          validate another user's ticket. So might need to pass the user's
     *          authentication details to valid tickets for users.
     *------------------------------------------------------------------------------
     **/    

    public function validate_alfresco_ticket($alf_ticket) {

        $url = $this->base_url . "/alfresco/s/api/login/ticket/" . $alf_ticket . "?alf_ticket=" . $alf_ticket;
        $result = $this->doGet($url);
        $json_result = $this->parse_xml_to_json($result->body);
        $result_ticket_decoded = json_decode($json_result);

        foreach ($result_ticket_decoded as $values) {
            $final_ticket_obtained = $values;
        }

        if ($final_ticket_obtained != '') {
            $this->_log->info('validate_alfresco_ticket', "Ticket is valid=$final_ticket_obtained");
            return $final_ticket_obtained;
        } else {
            $this->_log->warn('validate_alfresco_ticket', "Ticket is invalid=$alf_ticket");
            return FALSE;
        }

    }

    /**
     *------------------------------------------------------------------------------
     * Check that an Alfresco group exists.
     *------------------------------------------------------------------------------
     * @param   Alfresco admin ticket (ticket of connection user)
     * @param   Alfresco group name
     * @return  TRUE if group exists
     *          FALSE if group does not exist
     *------------------------------------------------------------------------------
     **/

    public function check_alfresco_group_exists($admin_ticket, $group_name) {

        $this->_log->debug('check_alfresco_group_exists', "Using admin ticket=$admin_ticket");
        $this->_log->info('check_alfresco_group_exists', "Checking alfresco for group=$group_name");
        
        $url = $this->base_url . '/alfresco/service/api/groups/' . rawurlencode($group_name);
        $this->auth_options = array('alf_ticket' => $admin_ticket);
        $result = $this->doRequest($url);
        if ($result->code == 200) {
            $groupsdata = json_decode($result->body);
            if ($groupsdata->data->displayName != '') {
                $this->_log->info('check_alfresco_group_exists', "Alfresco group exists=$group_name");
                return true;
            }
        }
        $this->_log->warn('check_alfresco_group_exists', "Alfresco group does not exist=$group_name");
        return false;
    }

    /**
     *------------------------------------------------------------------------------
     * Create a user in Alfresco if they do not exist using a unique Alfresco
     * username.
     *------------------------------------------------------------------------------
     * @param   Admin Alfresco Ticket
     * @param   User's Alfresco Group (based on their Moodle Role)
     * @return  TRUE if user was created (JSON result is true)
     *          FALSE if user was not created
     *------------------------------------------------------------------------------
     * @todo    Replace the hardcoded password with a randomised password
     *------------------------------------------------------------------------------
     **/    

    public function create_user($admin_ticket, $group_name) {

        global $USER, $DB;
        
        $alfresco_user['userName'] = $this->get_unique_alfresco_username();
        $alfresco_user['password'] = 'Imfinity123#'; // Very GK, fix me pls
        $alfresco_user['firstName'] = $USER->firstname;
        $alfresco_user['lastName'] = $USER->lastname;
        $alfresco_user['email'] = $USER->email;

        if ($admin_ticket != '' && $admin_ticket != '0') {

            if ($alfresco_user['userName'] && $alfresco_user['firstName'] && 
                $alfresco_user['lastName'] && $alfresco_user['email'] && $alfresco_user['password']) {

                $this->auth_options = array('alf_ticket' => $admin_ticket);
                
                $result = $this->doPost($this->peopleUrl, json_encode($alfresco_user), 'application/json');

                if ($result->code == 200) {
                    $this->_log->info('create_user', "Create user request successful for=".$alfresco_user['userName']);
                    if (!empty($group_name)) {
                        $url = $this->base_url . "/alfresco/service/api/groups/" . rawurlencode($group_name) . "/children/" . $alfresco_user['userName'];
                        $result = $this->doPost($url, '{}', 'application/json');
                        if ($result->code == 200) {
                            $this->_log->info('create_user', "Added user=".$alfresco_user['userName']. " to group=".$group_name);
                        }
                        $this->_log->error('create_user', "Unable to add user to group=".$group_name." error code=$result->code");
                    }
                    // update username to moodle alfrescousername(the one we created)                        
                    return json_decode($result->body);
                }
                $this->_log->error('create_user', "Create user request failed with HTTP code=$result->code");
            }
            $this->_log->error('create_user', "User data incomplete, cannot create user=".print_r($alfresco_user, true));
        }
        $this->_log->error('create_user', "Unable to create Alfresco user, invalid admin ticket=$admin_ticket");
        return false;
    }

    /**
     *------------------------------------------------------------------------------
     * Check if the specified username exists in Alfresco
     *------------------------------------------------------------------------------
     * @param   Admin Alfresco Ticket
     * @param   Alfresco Username
     * @return  TRUE if user exists in Alfresco
     *          FALSE if user does not exist or request failed
     *------------------------------------------------------------------------------
     **/

    public function user_exists_in_alfresco($admin_ticket, $alfresco_username) {

        $options['alf_ticket'] = $admin_ticket;
        $peopleURL = $this->peopleUrl . '/' . $alfresco_username;
        $result = $this->doRequest($peopleURL);

        if ($result->code == 200) {
            $userdata = json_decode($result->body);
            if ($userdata->userName == $alfresco_username) {
                return TRUE;
            }
            else {
                $this->_log->warn('user_exists_in_alfresco', "Unable to find user=$alfresco_username in alfresco");
                return FALSE;
            }
        }
        $this->_log->error('user_exists_in_alfresco', "Request failed with result code=$result->code");
        return FALSE;
    }

    /**
     *------------------------------------------------------------------------------
     * Create an Alfresco group
     *------------------------------------------------------------------------------
     * @param   Admin Alfresco Ticket
     * @param   Alfresco group name
     * @return  TRUE if Alfresco group created
     *          FALSE if Alfresco group not created or request failed
     *------------------------------------------------------------------------------
     * @todo    Further validation of group creation.
     *------------------------------------------------------------------------------
     **/        

    function create_alfresco_group($admin_ticket, $group_name) {

        $this->auth_options = array('alf_ticket' => $admin_ticket);
        $url = $this->base_url . "/alfresco/service/api/rootgroups/" . rawurlencode($group_name);
        $this->doPost($url, '{}', 'application/json');

        if ($result->code == 200) {
            // Should probably be parsing the JSON result checking for text that
            // indicates the group was successfully created here. Further validation
            // required. Have added to logs to see what the JSON result looks like to 
            // determine what to check for e.g. see how create_user does it's checking.
            $this->_log->info('create_alfresco_group', "Group creation result body (JSON)=$result->body");

            // Probably a bit premature until validation is added above
            $this->_log->info('create_alfresco_group', "Alfresco group=$group_name successfully created");
            return TRUE;
        }
        $this->_log->error('create_alfresco_group', "Request failed with result code=$result->code");
        return FALSE;
    }

    /**
     *------------------------------------------------------------------------------
     * Check if the Alfresco ticket is valid.
     *------------------------------------------------------------------------------
     * @param    Admin Alfresco Ticket
     * @return   TRUE if Ticket is valid
     *           FALSE if Ticket is invalid
     *------------------------------------------------------------------------------
     * @todo     Rewrite this. It will only work for the admin ticket. Instead, 
     *           another way to validate the ticket is to hit the user API:
     *           /alfresco/s/api/people/<username> with the user's ticket then
     *           check if you get a valid result (e.g. returns their userName). 
     *           that way, you know you have a valid Admin OR user ticket.
     *------------------------------------------------------------------------------
     * @internal  I don't see anywhere where this current function is even
     *            used, so going to leave it "as is" for now. But really any
     *            code that works with tickets should go via this function to ensure
     *            it is dealing with a valid ticket first.
     *------------------------------------------------------------------------------
     **/

    public function is_valid_alfresco_ticket($alf_ticket) {

        $url = $this->base_url . "/alfresco/s/api/login/ticket/" . $alf_ticket . "?alf_ticket=" . $alf_ticket;
        $result = $this->doGet($url);
        if($result->code == 200){
            return true;
        }
        else{
            return false;
        }
       
    }

    /**
     *------------------------------------------------------------------------------
     * Generate an Alfresco Username from the Moodle instance
     * using the following algorithm:
     * - Moodle username ($USER->username)
     * - "."
     * - Anything after :// in $CFG->wwwroot up to 40 characters
     *------------------------------------------------------------------------------
     * @param   None
     * @return  Unique Alfresco Username
     *------------------------------------------------------------------------------
     **/

    public function get_unique_alfresco_username() {        

        global $USER, $CFG;

        $exclusions = array('www.', 'moodle.', 'lms.', 'elearning.', 'learning.', 'online.');
        $skip_from_exclusion = array('elearning.trainingvc'); // Yes hardcoded in here :(
        $tlds = array('.com', '.edu', '.info', '.net', '.org'); // Adjust as required

        $suffix = $CFG->wwwroot;
        $this->_log->debug('get_unique_alfresco_username', "Starting suffix (wwwroot)=$suffix");

        // Remove protocol
        if (strpos($suffix, ':')) {
            $pos_after_protocol = strpos($suffix, ':') + 3;
            $suffix = substr($suffix, $pos_after_protocol, strlen($suffix));
            $this->_log->debug('get_unique_alfresco_username', "Suffix remove protocol=$suffix");
        }        
        
        // Remove trailing slash
        if (substr($suffix, -1) === '/') {            
            $suffix = substr($suffix, 0, strlen($suffix) - 1);
            $this->_log->debug('get_unique_alfresco_username', "Suffix remove trailing slash=$suffix");
        }        

        // Two scenarios:
        // (1) This is multihomed, so use everything after the next / after the protocol
        //     e.g. http://androgogic.com/example -> example
        // (2) This is a single instance, in which case, use everything up to the domain
        //     e.g. http://example.androgogic.com.au -> example
        
        if (strpos($suffix, '/')) { 
            $suffix = strstr($suffix, '/');
            $suffix = substr($suffix, 1, strlen($suffix));
            $this->_log->debug('get_unique_alfresco_username', "Multihome Suffix=$suffix");
        }
        else {          
            foreach($tlds as $tld) {                
                if ($domain_pos = strpos($suffix, $tld)) {
                    $suffix = substr($suffix, 0, $domain_pos);
                    $this->_log->debug('get_unique_alfresco_username', "Single Instance Suffix=$suffix");
                    break;
                }
            }           
        }        

        // Exclude certain generic words in $exclusions unless the suffix has
        // been flagged to skip from exclusion
        // e.g. http://moodle.androgogic.com.au -> androgogic (not moodle.androgogic)
        // e.g. http://www.androgogic.com -> androgogic (not moodle.androgogic)
        // e.g. http://elearning.example.com -> exammple (not elearning.example)

        if (!in_array($suffix, $skip_from_exclusion)) {
        foreach($exclusions as $exclusion) {            
                // Check we don't want to skip from exclusion        
            if (preg_match('/\b'.$exclusion.'/', $suffix)) {
                $suffix = str_replace($exclusion, '', $suffix);
                // Remove any "double dots .." after above if that happens
                $this->_log->debug('get_unique_alfresco_username', "Suffix removed exclusions=$suffix");
            }
        }
        }       

        // If there is still more than one segment, get the first one
        if (strpos($suffix, '.')) {
            $suffix = strstr($suffix, '.', TRUE);   
            $this->_log->debug('get_unique_alfresco_username', "Suffix get first segement=$suffix");
        }
        
        $unique_alfresco_username = $USER->username . "." . $suffix;
        $unique_alfresco_username = str_replace('..', '.', $unique_alfresco_username);

        $this->_log->info('get_unique_alfresco_username', "Unique username=$unique_alfresco_username");

        return $unique_alfresco_username;

    }

    /**
     *------------------------------------------------------------------------------
     * Determine user's group from moodle role and return as Alfresco group name
     *------------------------------------------------------------------------------
     * @param   Andresco repository options
     * @return  Alfresco group name
     *------------------------------------------------------------------------------
     **/

    function get_user_alfresco_group($andresco_options) {
 
        global $COURSE, $USER;

        $group_name = '';

        $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);
        $contextsys = get_context_instance(CONTEXT_SYSTEM);

        if (!empty($andresco_options['alfresco_username_default']) && strlen(preg_replace('/\s+/', '', $andresco_options['alfresco_username_default'])) > 0) {
            $group_name_default = trim($andresco_options['alfresco_username_default']);        
            $this->_log->debug('get_user_alfresco_group', "Setting default group name=$group_name_default");
        } else {
            $group_name_default = 'Students';
            $this->_log->debug('get_user_alfresco_group', "Using default group name=$group_name_default");
        }

        if (is_siteadmin($USER->id)) {
            if (!empty($andresco_options['alfresco_username_admin']) && strlen(preg_replace('/\s+/', '', $andresco_options['alfresco_username_admin'])) > 0) {
                $group_name = trim($andresco_options['alfresco_username_admin']);
                $this->_log->debug('get_user_alfresco_group', "Site admin group name=$group_name");
            } else {
                $group_name = $group_name_default;                
            }
        } else {
            $students = get_role_users(5, $context);
            $editing_teacher = get_role_users(3, $context);
            $usersysroles = get_user_roles($contextsys, $USER->id);
            $rolenames = array();
            foreach ($usersysroles as $usersysrole) {
                array_push($rolenames,$usersysrole->shortname);
            }

            if (in_array("pahigh", $rolenames)) { // eworks specific
                if (!empty($andresco_options['alfresco_username_admin']) && strlen(preg_replace('/\s+/', '', $andresco_options['alfresco_username_admin'])) > 0) {
                    $group_name = trim($andresco_options['alfresco_username_admin']);
                    $this->_log->debug('get_user_alfresco_group', "PA high group name=$group_name");
                } else {
                    $group_name = $group_name_default;
                }
            } else if (array_key_exists($USER->id, $editing_teacher) || in_array("pa",$rolenames)) { // eworks specific
                if (!empty($andresco_options['alfresco_username_teacher']) && strlen(preg_replace('/\s+/', '', $andresco_options['alfresco_username_teacher'])) > 0) {
                    $group_name = trim($andresco_options['alfresco_username_teacher']);
                    $this->_log->debug('get_user_alfresco_group', "PA group name=$group_name");
                } else {
                    $group_name = $group_name_default;
                }
            } else {
                if (!empty($andresco_options['alfresco_username_default']) && strlen(preg_replace('/\s+/', '', $andresco_options['alfresco_username_default'])) > 0) {
                    $group_name = trim($andresco_options['alfresco_username_default']);
                    $this->_log->debug('get_user_alfresco_group', "Username group name=$group_name");
                } else {
                    $group_name = $group_name_default;
                }
            }
        }
        $this->_log->info('get_user_alfresco_group', "Group name=$group_name");
        return $group_name;
    }

    /**
     *------------------------------------------------------------------------------
     * Get a user ticket for the relevant Moodle user. Also set up their group
     * in Alfresco if it doesn't already exist to ensure that user has the
     * appropriate security
     *------------------------------------------------------------------------------
     * @param   Andresco options
     * @return  User ticket (and set user ticket in session)
     *------------------------------------------------------------------------------
     * @todo    There probably isn't enough error handling around invalid tickets
     *          in this code, incorporate is_valid_alfresco_ticket() into it once
     *          that method has been coded correctly.
     *------------------------------------------------------------------------------
     **/

    function get_user_ticket($andresco_options, $repo_id) {

        global $SESSION;
        $this->_log->debug('get_user_ticket', "Andresco reppsitory ID=$repo_id");

        // First get an admin ticket
        $admin_ticket = $this->run_as_admin();
        $this->_log->debug('get_user_ticket', "Admin ticket=$admin_ticket");
        
        // Then get the user's alfresco group
        $group_name = $this->get_user_alfresco_group($andresco_options);
        $this->_log->debug('get_user_ticket', "Alfresco group name=$group_name");              

        // Process group name and create the group if we don't have one        
        if (!empty($group_name)) {
            $isGroupExist = $this->check_alfresco_group_exists($admin_ticket, $group_name);
            if ($isGroupExist == false) {
                $this->_log->debug('get_user_ticket', "Creating Alfresco Group=$group_name");                
                $this->create_alfresco_group($ticket, $group_name);
            }
        }

        // Always try to generate a new ticket. Alfresco is smart about this, if there is an
        // existing valid ticket it will return it, and only regenerate if required.
        // Timing is quick on this (have tested using microtime) and not a performance hit

        $user_ticket = $this->authenticate_alfresco_user($admin_ticket, $group_name);
        $this->_log->info('get_user_ticket', "User ticket=$user_ticket");
        
        if ($user_ticket != '' && $user_ticket != '0') {
            $this->_log->info('get_user_ticket', "Using existing session ticket=$user_ticket");                
            $SESSION->{$this->sessname}[$repo_id] = $user_ticket;
            return $user_ticket;
        }
        else {
            $this->_log->error('get_user_ticket', "Unable to get a user ticket");            
            return FALSE;
        }

    }

    /**
     *------------------------------------------------------------------------------
     * Helper to Parse XML to JSON format.
     *------------------------------------------------------------------------------
     * @param   XML input
     * @return  JSON output
     *------------------------------------------------------------------------------
     **/    

    public function parse_xml_to_json($xml) {
        $fileContents = str_replace(array("\n", "\r", "\t"), '', $xml);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $simpleXml = simplexml_load_string($fileContents);
        $json = json_encode($simpleXml);
        return $json;        
    }

}

// end of repository/andresco_cmis/lib/userlib.php
