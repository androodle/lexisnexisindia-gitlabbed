<?php

/**
 * ------------------------------------------------------------------------------
 * Andresco URL Rewriter
 * ------------------------------------------------------------------------------
 * The index page takes a URL with appropriate parameters and translates it
 * into a URL for auth.php (Alfresco side) with dynamic moodle parameters.
 *
 * @param 	Andresco repository id (required)
 * @param   Alfresco content UUID (required)
 * @param   Alfresco content version
 * @param 	Alfresco content filename
 * @param 	Debug mode (optional, does not redirect)
 * @return 	Array of repository configuration values if found
 * 			FALSE otherwise
 *
 * ------------------------------------------------------------------------------ 
 * */
require_once("../../config.php");

// The repository ID that this URL relates to
$repo = required_param('repo', PARAM_INT);

// The UUID of the Alfresco content
$uuid = required_param('uuid', PARAM_RAW);

// The version number of the Alfresco content
$version = optional_param('version', '', PARAM_RAW);

// The Filename of the content
$filename = optional_param('filename', '', PARAM_RAW);

// The base component of the URL in Alfresco.
$base = optional_param('base', '', PARAM_RAW);

// Debug parameter
$debug = optional_param('debug', 0, PARAM_INT);

if ($debug) {
    echo '<h1>Debug Mode</h1>';
}

// Get repository details
if (!$repository_config = get_repository_config($repo, $debug)) {
    print_error('invalidandrescorepository', 'repository_andresco_cmis');
    die();
}

// Generate base URL
$cmis_url = $repository_config['alfresco_cmis_url'];
$base_url = substr($cmis_url, 0, strpos($cmis_url, '/alfresco/s/cmis')) . '/';

// Append auth.php location
$auth_url = $repository_config['andresco_auth'] ? $repository_config['andresco_auth'] : 'auth.php';

// Generate parameters

$params = '?uuid=' . $uuid;

// If version parameter is passed (either latest or a specific version) then use it
if ($version && !empty($version)) {
    $params .='&version=' . $version;
}

// If no version parameter is passed then assume latest
if ($version && empty($version)) {
    $params .='&version=latest';
}

// changed from fullname -> wwwroot for avoiding duplication, mjf 24/12/13
$moodlename = $CFG->dbname;
if (isset($CFG->wwwroot)) {
    $moodlename = $CFG->wwwroot;
}

$params .= "&moodlename=" . urlencode($moodlename);

$courseid = 0;
if (isset($USER->currentcourseaccess)) {
    $courses = $USER->currentcourseaccess;
    $last_course = array_slice($courses, -1, 1, TRUE);
    $courseid = key($last_course);
}
if (!$courseid) {
    $courseid = 0;
}
$params .= "&courseid=" . urlencode($courseid);

$courseshortname = 'N/A';
if ($courseid > 0) {
    $q = 'select shortname from {course} where id = ?';
    $r = $DB->get_record_sql($q, array('id' => $courseid));
    $courseshortname = $r->shortname;
}
$params .= '&courseshortname=' . urlencode($courseshortname);

$userid = 0;
if (isset($USER->id)) {
    $userid = $USER->id;
}
$params .= "&userid=" . urlencode($userid);

$username = 'unknown';
if (isset($USER->username)) {
    $username = $USER->username;
}
$params .= "&username=" . urlencode($username);

// If base parameter is passed then use it.
if ($base && !empty($base)) {
    $params .='&base=' . urlencode($base);
}

// Filename parameter is added last to help Moodle with determining mime-type
// from file extension and in some cases like TinyMCE, used to validate content.
if ($filename && !empty($filename)) {
    $params .='&filename=' . $filename;
}
//Fix for FS#247
$alf_ticket = generate_alfresco_ticket($repository_config['alfresco_cmis_url'], $repository_config['connection_username'], $repository_config['connection_password']);
if($alf_ticket) {
	$params .='&ticket=' . $alf_ticket;
}
//Fix for FS#247 ends here

$final_url = $base_url . $auth_url . $params;
if ($debug) {
    echo '<p>Final URL = ' . $final_url . '</p>';
}

// Redirect to auth.php URL
redirect($final_url);

/**
 * ------------------------------------------------------------------------------
 * Get Repository Configuration
 * ------------------------------------------------------------------------------
 * Get repository configuration for the given repository id from the database.
 *
 * @param 	repository id
 * @return 	Array of repository configuration values if found
 * 			FALSE otherwise
 *
 * ------------------------------------------------------------------------------ 
 * */
function get_repository_config($repo = 0, $debug = 0) {

    global $DB, $CFG;

    if ($repo > 0) {
        $q = 'select * from {repository_instance_config} where instanceid = ?';
        $r = $DB->get_records_sql($q, array('instanceid' => $repo));

        $repository_config = array();
        foreach ($r as $property) {
            $repository_config[$property->name] = $property->value;
            if ($property->name == "connection_password") {
                $repository_config[$property->name] = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $CFG->passwordsaltmain, base64_decode($property->value), MCRYPT_MODE_ECB), "\0");
            }
        }

        if ($debug) {
            echo '<pre>';
            echo "<p>Repository properties for repo_id=$repo</p>";
            var_dump($repository_config);
        }

        return $repository_config;
    } else {
        return FALSE;
    }
}
//Fix for FS#247
/**
 * Generate Alfresco Ticket
 * @param $server_url
 * @param $username
 * @param $password
 * @return Alfresco ticket
 */
function generate_alfresco_ticket($server_url, $username, $password) {
	$api_url = str_replace('cmis', 'api/login', $server_url);
	$post_data = json_encode(array("username" => $username, "password" => $password));
	$ticket = '';
	try {
		$ch = curl_init($api_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($post_data))
		);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$curl_response = curl_exec($ch);
		curl_close($ch);
		$ticket = json_decode($curl_response)->data->ticket;
	} catch (Exception $e) {
		throw new Exception($e->getMessage());
	}
	return $ticket;
}
//Fix for FS#247 ends here
// end of repository/andresco_cmis/index.php
