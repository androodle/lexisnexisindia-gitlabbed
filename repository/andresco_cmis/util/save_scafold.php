<?php

require('../../../config.php');
global $USER, $DB;
$j = 0;
if (isset($_POST['weightage'])) {
    $totalweightage = count($_POST['weightage']);
}
$rid = $_GET['rid'];
$DB->delete_records('scafold_ui', array('repository_instance_id' => $rid));
if (isset($_POST['weightage'])) {
    foreach ($_POST['weightage'] as $key => $values) {
        $i = $values;
        $data = new stdClass();
        $data->property_name = $_POST['property_name_' . $i];
        $data->display_label = $_POST['display_label_' . $i];
        if (isset($_POST['is_constraint_' . $i])) {
            if ($_POST['is_constraint_' . $i] == 'on' || $_POST['is_constraint_' . $i] == '1') {
                $data->is_constraint = '1';
            } else {
                $data->is_constraint = '0';
            }
        }
        //$data->is_constraint = $values['is_constraint_'.$i];
        if (isset($_POST['default_value_expression_' . $i])) {
        $data->default_value_expression = $_POST['default_value_expression_' . $i];
        }
        else {
            $data->default_value_expression = '';
        }
        
        $data->render_type = $_POST['render_type_' . $i];
        //$data->is_mandatory = $values['is_mandatory_'.$i];
        if (isset($_POST['is_mandatory_' . $i])) {
            if ($_POST['is_mandatory_' . $i] == 'on' || $_POST['is_mandatory_' . $i] == '1') {
                $data->is_mandatory = '1';
            } else {
                $data->is_mandatory = '0';
            }
        }
        $data->weightage = $j;
        $data->repository_instance_id = $rid;
        $data->created_by = $USER->id;
        $data->updated_by = $USER->id;
        $data->created = time();
        $data->updated = time();
        $data->id = $DB->insert_record('scafold_ui', $data);
        $j++;
    }
}
//$returnurl = new moodle_url('/repository/andresco_cmis/util/add_scafold.php?rid='.$rid);
$sess_key = $USER->sesskey;
$returnurl = new moodle_url('/admin/repository.php?sesskey=' . $sess_key . '&action=edit&repos=andresco_cmis');
redirect($returnurl);
?>
