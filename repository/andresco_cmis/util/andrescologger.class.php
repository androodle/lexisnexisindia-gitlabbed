<?php

/**
 *-----------------------------------------------------------------------------
 * Class to log actions within in Andresco to assist with troubleshooting
 * and support of the Andresco repository plugin.
 *-----------------------------------------------------------------------------
 * @copyright	2014+ Androgogic Pty Ltd
 * @author 		http://www.androgogic.com.au
 *-----------------------------------------------------------------------------
 **/

defined('MOODLE_INTERNAL') || die();

class AndrescoLogger {

	private $_log_src;
	private $_log_level;
	private $_log_dir;
	private $_log_file;
	private $_log_path;

	// Andresco Log levels

	// These translate to numeric values so we can do things like 
	// ANDRESCO_LOG_ERROR > ANDRESCO_LOG_DEBUG to determine what 
	// level of logging to use. The lower the log level, the more 
	// verbose the logging.

	const ANDRESCO_LOG_ERROR = 400;
	const ANDRESCO_LOG_WARN  = 300;
	const ANDRESCO_LOG_INFO  = 200;
	const ANDRESCO_LOG_DEBUG = 100;

	/**
	 *-----------------------------------------------------------------------------
	 * Constructor to set up logging
	 *-----------------------------------------------------------------------------
	 * Set up log directory, log file, log path and log level
	 *-----------------------------------------------------------------------------
	 **/

	public function __construct($src = 'unknown') {

		global $CFG;

		// Set the log source
		$this->_log_src = $src;

		// Set the log level. 	
		// @todo Look at making this something that can be set through Andresco configuration
		// Using log level debug at the moment to help with troubleshooting Andresco issues
		$this->_log_level = $this::ANDRESCO_LOG_INFO;

		// Log to the dataroot
		$this->_log_dir = $CFG->dataroot;

		// Set the log file name
		$this->_log_file = 'andresco.log';

		// Set the full log path - not slash may need adjusting if not Unix
		$this->_log_path = $this->_log_dir . '/' . $this->_log_file;

		// Check if we have a log file already. If we don't create it
		if (!file_exists($this->_log_path)) {
			try {
				file_put_contents($this->_log_path, "--- Andresco Log ---\n\n");
			}
			catch (Exception $e) {
				throw new Exception("Unable to create file: " . $this->_log_path);
			}
		}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Log a message to the Andresco Log file.
	 *-----------------------------------------------------------------------------
	 * @param  func is the function from which the log message was called
	 * @param  msg is the text to log
	 * @param  level is the log level
	 * @return true if logged, false if unable to log (e.g. above threshold)
	 *-----------------------------------------------------------------------------
	 **/

	public function log($func, $msg, $level = NULL) {

		if (empty($level)) {
			$level = $this::ANDRESCO_LOG_ERROR; // Only log errors by default
		}

		// Only log if we are within the log level threshold
		if ($level >= $this->_log_level) {

			$log_line  = "[" . date('Y-m-d h:i:s', time()) . "] "; // e.g [2014-03-26 08:08:00]
			$log_line .= $this->log_level_as_string($level) . ": "; // e.g. ERROR
			$log_line .= "[" . $this->_log_src . "->" . $func . "] "; // e.g. [lib.php->__construct]
			$log_line .= $msg; // e.g. this is a log entry
			$log_line .= "\n";

			file_put_contents($this->_log_path, $log_line, FILE_APPEND);
		}		

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Log Debug Messages
	 *-----------------------------------------------------------------------------
	 * @param  func is the function from which the log message was called
	 * @param  msg is the text to log
	 * @return true if logged, false if unable to log (e.g. above threshold)
 	 *-----------------------------------------------------------------------------
	 **/

	public function debug($func, $msg) {
		return $this->log($func, $msg, $this::ANDRESCO_LOG_DEBUG);
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Log Information Messages
	 *-----------------------------------------------------------------------------
	 * @param  func is the function from which the log message was called
	 * @param  msg is the text to log
	 * @return true if logged, false if unable to log (e.g. above threshold)
 	 *-----------------------------------------------------------------------------
	 **/

	public function info($func, $msg) {
		return $this->log($func, $msg, $this::ANDRESCO_LOG_INFO);
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Log Warning Messages
	 *-----------------------------------------------------------------------------
	 * @param  func is the function from which the log message was called
	 * @param  msg is the text to log
	 * @return true if logged, false if unable to log (e.g. above threshold)
 	 *-----------------------------------------------------------------------------
	 **/

	public function warn($func, $msg) {
		return $this->log($func, $msg, $this::ANDRESCO_LOG_WARN);
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Log Error Messages
	 *-----------------------------------------------------------------------------
	 * @param  func is the function from which the log message was called
	 * @param  msg is the text to log
	 * @return true if logged, false if unable to log (e.g. above threshold)
 	 *-----------------------------------------------------------------------------
	 **/

	public function error($func, $msg) {
		return $this->log($func, $msg, $this::ANDRESCO_LOG_ERROR);
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Get the log level as a human readable string
	 *-----------------------------------------------------------------------------
	 * @param  log level as a constant
	 * @return log level as a string (e.g. ERROR, WARN, INFO, DEBUG)
	 *-----------------------------------------------------------------------------
	 **/

	private function log_level_as_string($level) {
		switch ($level) {
			case $this::ANDRESCO_LOG_ERROR:
				return 'ERROR';
				break;
			case $this::ANDRESCO_LOG_WARN:
				return 'WARN';
				break;
			case $this::ANDRESCO_LOG_INFO:
				return 'INFO';
				break;
			case $this::ANDRESCO_LOG_DEBUG:
				return 'DEBUG';
				break;
			default:
				return 'UNKNOWN';			
		}
	}

}

// end of repository/andresco_cmis/util/andrescologger.class.php
