<?php

/**
 * get the course category name and category path name for FS#227
 * used by parse_variables.php and lib.php
 * @return Alfresco course object containing course category name and path
 */
function create_andrescourse() {
	global $COURSE, $CFG;

	$list = $parents = array();
	include_once($CFG->dirroot.'/course/lib.php');
	make_categories_list($list, $parents, 0, 0, 0, 0);  // use moodle function to get category path list

	$path = $list[$COURSE->category ? $COURSE->category : 1]; // for custom property configuration only, without category

	$ANDRESCOCOURSE = new stdclass();

	if (isset($path) & $path != '') {
		$ANDRESCOCOURSE->categorypathname = $path;
	}
	else {
		$ANDRESCOCOURSE->categorypathname = 'Not available';
	}
	$categoryname = trim(substr($path, (strrpos($path, '/'))));
	$ANDRESCOCOURSE->categoryname = substr($categoryname, 0, 1) == '/' ?  substr($categoryname, 1) : $categoryname;

	return $ANDRESCOCOURSE;
}
