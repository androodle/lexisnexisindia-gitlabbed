<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains general functions for the Androgogic Tiles course format
 *
 * This file is derived from work Copyright 2009 by Sam Hemelryk and distributed
 * as part of Moodle.
 *
 * It contains modifications for the Tile by Greg Newton, Androgogic Pty, Ltd, 2013
 *
 * @since 2.0
 * @package    format
 * @subpackage tiles
 * @copyright 2009 Sam Hemelryk
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/course/format/tiles/locallib.php');
require_once($CFG->dirroot . '/course/format/lib.php');

class format_tiles extends format_base {

    /**
     * Indicates this format uses sections.
     *
     * @return bool Returns true
     */
    public function uses_sections() {
        return true;
    }

    /**
     * Used to display the course structure for a course where format=topic
     *
     * This is called automatically by {@link load_course()} if the current course
     * format = weeks.
     *
     * @param array $path An array of keys to the course node in the navigation
     * @param stdClass $modinfo The mod info object for the current course
     * @return bool Returns true
     */
    public function load_content(&$navigation, $course, $coursenode) {
        return $navigation->load_generic_course_sections($course, $coursenode, 'tiles');
        #return $navigation->load_course_sections($course, $coursenode);
    }

#    public function get_section_name($course, $section) {
#        return format_tiles_get_section_name($course, $section);
#    }
    #TODO: Go back and alter format_tiles_get_section_name
    public function get_section_name($section) {
        $section = $this->get_section($section);
        if ((string)$section->name !== '') {
            return format_string($section->name, true,
                    array('context' => context_course::instance($this->courseid)));
        } else if ($section->section == 0) {
            return get_string('section0name', 'format_tiles');
        } else {
            return get_string('topic').' '.$section->section;
        }
    }

    /**
     * The URL to use for the specified course (with section)
     *
     * @param int|stdClass $section Section object from database or just field course_sections.section
     *     if omitted the course view page is returned
     * @param array $options options for view URL. At the moment core uses:
     *     'navigation' (bool) if true and section has no separate page, the function returns null
     *     'sr' (int) used by multipage formats to specify to which section to return
     * @return null|moodle_url
     */
    public function get_view_url($section, $options = array()) {
        $course = $this->get_course();
        $url = new moodle_url('/course/view.php', array('id' => $course->id));

        $sr = null;
        if (array_key_exists('sr', $options)) {
            $sr = $options['sr'];
        }
        if (is_object($section)) {
            $sectionno = $section->section;
        } else {
            $sectionno = $section;
        }
        if ($sectionno !== null) {
            #if ($sr !== null) {
            #    if ($sr) {
                    $usercoursedisplay = COURSE_DISPLAY_MULTIPAGE;
            #        $sectionno = $sr;
            #    } else {
            #        $usercoursedisplay = COURSE_DISPLAY_SINGLEPAGE;
            #    }
            #} else {
            #    $usercoursedisplay = $course->coursedisplay;
            #}
            if ($sectionno != 0 && $usercoursedisplay == COURSE_DISPLAY_MULTIPAGE) {
#                $url->param('section', $sectionno);
                $url->param('topic', $sectionno);
            } else {
                if (!empty($options['navigation'])) {
                    return null;
                }
                $url->set_anchor('topic-'.$sectionno);
#                $url->set_anchor('section-'.$sectionno);
            }
        }
        return $url;
    }

    /**
     * Declares support for course AJAX features
     *
     * @see course_format_ajax_support()
     * @return stdClass
     */
    public function supports_ajax() {
        $ajaxsupport = new stdClass();
        $ajaxsupport->capable = false;
        $ajaxsupport->testedbrowsers = array('MSIE' => 6.0, 'Gecko' => 20061111, 'Safari' => 531, 'Chrome' => 6.0);
        return $ajaxsupport;
    }

    /**
     * Definitions of the additional options that this course format uses for course
     *
     * Topics format uses the following options:
     * - coursedisplay
     * - numsections
     * - hiddensections
     *
     * @param bool $foreditform
     * @return array of options
     */
    public function course_format_options($foreditform = false) {
        static $courseformatoptions = false;

        static $formatconfig = false;
        if ($formatconfig === false) {
            $formatconfig = get_config('format_tiles');
        }

        if ($courseformatoptions === false) {
            $courseconfig = get_config('moodlecourse');
            $courseformatoptions = array(
                'numsections' => array(
                    'default' => $courseconfig->numsections,
                    'type' => PARAM_INT,
                ),
                'hiddensections' => array(
                    'default' => $courseconfig->hiddensections,
                    'type' => PARAM_INT,
                ),
/*                'coursedisplay' => array(
                    'default' => $courseconfig->coursedisplay,
                    'type' => PARAM_INT,
                ),*/
                'bottomtabs' => array(
                    'default' => $formatconfig->bottom_tabs_default,
                    'type' => PARAM_BOOL,
                ),
                'allsections' => array(
                    'default' => $formatconfig->all_sections_default,
                    'type' => PARAM_BOOL,
                ),
                'titletabs' => array(
                    'default' => $formatconfig->allow_title_tabs,
                    'type' => PARAM_BOOL,
                ),
            );
        }
        if ($foreditform && !isset($courseformatoptions['coursedisplay']['label'])) {
            $courseconfig = get_config('moodlecourse');
            $max = $courseconfig->maxsections;
            if (!isset($max) || !is_numeric($max)) {
                $max = 52;
            }
            $sectionmenu = array();
            for ($i = 0; $i <= $max; $i++) {
                $sectionmenu[$i] = "$i";
            }
            $courseformatoptionsedit = array(
                'numsections' => array(
                    'label' => new lang_string('numberweeks'),
                    'element_type' => 'select',
                    'element_attributes' => array($sectionmenu),
                ),
                'hiddensections' => array(
                    'label' => new lang_string('hiddensections'),
                    'help' => 'hiddensections',
                    'help_component' => 'moodle',
                    'element_type' => 'select',
                    'element_attributes' => array(
                        array(
                            #0 => new lang_string('hiddensectionscollapsed'),
                            1 => new lang_string('hiddensectionsinvisible')
                        )
                    ),
                ),
/*                'coursedisplay' => array(
                    'label' => new lang_string('coursedisplay'),
                    'element_type' => 'select',
                    'element_attributes' => array(
                        array(
                            #COURSE_DISPLAY_SINGLEPAGE => new lang_string('coursedisplay_single'),
                            COURSE_DISPLAY_MULTIPAGE => new lang_string('coursedisplay_multi')
                        )
                    ),
                    'help' => 'coursedisplay',
                    'help_component' => 'moodle',
                ),*/
                'bottomtabs' => array(
                    'label' => new lang_string('showbottomtabs', 'format_tiles'),
                    'help' => 'showbottomtabs',
                    'help_component' => 'format_tiles',
/*                    'element_type' => 'checkbox',
                    'element_attributes' => array(
                    )*/
                    'element_type' => 'select',
                    'element_attributes' => array(
                        array(
                            0 => new lang_string('hide', 'format_tiles'),
                            1 => new lang_string('show', 'format_tiles')
                        )
                    ),
                ),
                'allsections' => array(
                    'label' => new lang_string('showallsections', 'format_tiles'),
                    'help' => 'showallsections',
                    'help_component' => 'format_tiles',
/*                    'element_type' => 'checkbox',
                    'element_attributes' => array(
                    )*/
                    'element_type' => 'select',
                    'element_attributes' => array(
                        array(
                            0 => new lang_string('hide', 'format_tiles'),
                            1 => new lang_string('show', 'format_tiles')
                        )
                    ),
                ),
                'titletabs' => array(
                    'label' => new lang_string('showtitlesintabs', 'format_tiles'),
                    'help' => 'showtitlesintabs',
                    'help_component' => 'format_tiles',
/*                    'element_type' => 'checkbox',
                    'element_attributes' => array(
                    )*/
                    'element_type' => 'select',
                    'element_attributes' => array(
                        array(
                            0 => new lang_string('sectionnumber', 'format_tiles'),
                            1 => new lang_string('sectiontitle', 'format_tiles')
                        )
                    ),
                )
            );
            $courseformatoptions = array_merge_recursive($courseformatoptions, $courseformatoptionsedit);
        }
        return $courseformatoptions;
    }

    /**
     * Adds format options elements to the course/section edit form
     *
     * This function is called from {@link course_edit_form::definition_after_data()}
     *
     * @param MoodleQuickForm $mform form the elements are added to
     * @param bool $forsection 'true' if this is a section edit form, 'false' if this is course edit form
     * @return array array of references to the added form elements
     */
    public function create_edit_form_elements(&$mform, $forsection = false) {

        static $formatconfig = false;
        if ($formatconfig === false) {
            $formatconfig = get_config('format_tiles');
        }

        $elements = array();
        if ($forsection) {
            $options = $this->section_format_options(true);
        } else {
            $options = $this->course_format_options(true);
        }
        foreach ($options as $optionname => $option) {

            /* Do not display the optional features at the course if they are
             * disabled at the format plugin level. */
            if ($optionname == 'bottomtabs' && !$formatconfig->allow_bottom_tabs) {
                continue;
            }
            if ($optionname == 'allsections' && !$formatconfig->allow_all_sections_view) {
                continue;
            }
            if ($optionname == 'titletabs' && !$formatconfig->allow_title_tabs) {
                continue;
            }


            if (!isset($option['element_type'])) {
                $option['element_type'] = 'text';
            }
            $args = array($option['element_type'], $optionname, $option['label']);
            if (!empty($option['element_attributes'])) {
                $args = array_merge($args, $option['element_attributes']);
            }
            $elements[] = call_user_func_array(array($mform, 'addElement'), $args);
            if (isset($option['help'])) {
                $helpcomponent = 'format_'. $this->get_format();
                if (isset($option['help_component'])) {
                    $helpcomponent = $option['help_component'];
                }
                $mform->addHelpButton($optionname, $option['help'], $helpcomponent);
            }
            if (isset($option['type'])) {
                $mform->setType($optionname, $option['type']);
            }
            if (is_null($mform->getElementValue($optionname)) && isset($option['default'])) {
                $mform->setDefault($optionname, $option['default']);
            }
        }
        return $elements;
    }


    /**
     * Returns a URL to arrive directly at a section
     *
     * @param int $courseid The id of the course to get the link for
     * @param int $sectionnum The section number to jump to
     * @return moodle_url
     */
    function callback_tiles_get_section_url($courseid, $sectionnum) {
        return new moodle_url('/course/view.php', array('id' => $courseid, 'topic' => $sectionnum));
    }
}
