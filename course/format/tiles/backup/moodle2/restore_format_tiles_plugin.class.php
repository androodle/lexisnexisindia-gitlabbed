<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    format
 * @subpackage tiles
 * @author     Greg Newton, Androgogic <greg.newton@androgogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2014 Androgogic, Ltd.
 *
 * TODO: Description goes here
 */

/**
 * Restore plugin for the tiles format.
 */
class restore_format_tiles_plugin extends restore_format_plugin {
    /**
     * Returns the paths to be handled by the plugin at course level.
     */
    protected function define_course_plugin_structure() {

        // The course level component does not actually do anything at this
        // stage. Left as a placeholder for the time being. The actual meat of
        // this is in the sections.
        $paths = array();
        $paths[] = new restore_path_element('tiles', $this->get_pathfor('/'));
        return $paths;
    }

    /**
     * Process the course level element. Note: at present this is placeholder only.
     */
    public function process_tiles($data) {
        global $DB;

        $data = (object) $data;

        /* We only process this information if the course we are restoring to
          has 'tiles' format (target format can change depending of restore options). */
        $format = $DB->get_field('course', 'format', array('id' => $this->task->get_courseid()));
        if ($format != 'tiles') {
            return;
        }

        $data->courseid = $this->task->get_courseid();

        // Do nothing.
    }

    /**
     * Returns the paths to be handled by the plugin at section level
     */
    protected function define_section_plugin_structure() {

        $paths = array();
        $paths[] = new restore_path_element('tile_image', $this->get_pathfor('/'));
        return $paths; // And we return the interesting paths.
    }

    /**
     * Process the custom tile image for a section.
     */
    public function process_tile_image($data) {
        global $DB;

        $data = (object) $data;

        // Only add this information if the format is using the tiles format.
        // TODO: Review this. This is here based on examples and is take to be a
        // safety measure, but there may be a better way of doing this.
        $format = $DB->get_field('course', 'format', array('id' => $this->task->get_courseid()));
        if ($format != 'tiles') {
            return;
        }

        $data->courseid = $this->task->get_courseid();
        $data->sectionid = $this->task->get_sectionid();
        # The $data object contains 'filename', 'userid', 'timecreated', 'timemodified'.

        $existing = $DB->get_record('format_tiles_tile_image', array('courseid' => $data->courseid, 'sectionid' => $data->sectionid));
        if ($existing) {
            $existing->filename = filename;
            $existing->userid = userid;
            $existing->timecreated = timecreated;
            $existing->timemodified = timemodified;
            $DB->update_record('format_tiles_tile_image', $existing);
        } else {
            $format_tiles_tile_image_id = $DB->insert_record('format_tiles_tile_image', $data);
        }

/*

        if (!$DB->record_exists('format_tiles_tile_image', array('courseid' => $data->courseid, 'sectionid' => $data->sectionid))) {
            if (!$DB->insert_record('format_tiles_tile_image', $data, true)) {
                throw new moodle_exception('invalidrecordid', 'format_grid', '',
                'Could not insert icon. Grid format table format_tiles_tile_image is not ready. An administrator must visit the notifications section.');
            }
        } else {
            global $PAGE;
            $old = $DB->get_record('format_tiles_tile_image', array('courseid' => $data->courseid, 'sectionid' => $data->sectionid));
            if ((is_null($old->image)) && (strcmp($PAGE->pagetype, 'backup-restore') == 0)) {
                // Update the record to use this icon as we are restoring not importing and no icon exists already.
                $data->id = $old->id;
                if (!$DB->update_record('format_tiles_tile_image', $data)) {
                    throw new moodle_exception('invalidrecordid', 'format_grid', '',
                    'Could not update icon. Grid format table format_tiles_tile_image is not ready. An administrator must visit the notifications section.');
                }
            }
        }*/
    }
}
