<?php

/**
 * Internal functions for the Androgogic Tile format.
 *
 * May contain some code derived from the Moodle topics format from Moodle 2.2,
 * originally developed by N.D.Freear@open.ac.uk and others and Copyright 2006
 * The Open University.
 *
 * @since 2.0
 * @package    format
 * @subpackage tiles
 * @copyright 2013 Androgogic
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// --------------------------------------------------------------------------
// This function is used to work around diffences between Moodle 2.2 and 2.3

/**
 * Returns the course section to display or 0 meaning show all sections. Returns 0 for guests.
 * It also sets the $USER->display cache to array($courseid=>return value)
 *
 * @param int $courseid The course id
 * @return int Course section to display, 0 means all
 */
function format_tiles_course_get_display($courseid) {
    global $USER, $DB;

    if (!isloggedin() or isguestuser()) {
        //do not get settings in db for guests
        return 0; //return the implicit setting
    }

    if (!isset($USER->display[$courseid])) {
        if (!$display = $DB->get_field('course_display', 'display', array('userid' => $USER->id, 'course' => $courseid))) {
            $display = 0; // all sections option is not stored in DB, this makes the table much smaller
        }
        //use display cache for one course only - we need to keep session small
        $USER->display = array($courseid => $display);
    }

    return $USER->display[$courseid];
}

// --------------------------------------------------------------------------

/**
 * Get a section from the list of sections in a course by number. If it doesn't
 * exist (Moodle takes a just-in-time approach), create it.
 * @global type $DB
 * @param object $course The course (as an object)
 * @param array $sections The sections (as objects) associated with the course.
 * @param type $section_num The number of the section
 * @return \stdClass The section (as an object)
 */
function format_tiles_get_or_create_section($course, $sections, $section_num) {
    global $DB;

    if (!empty($sections[$section_num])) {
        $thissection = $sections[$section_num];
    } else {
        $thissection = new stdClass;
        $thissection->course = $course->id;   // Create a new section structure
        $thissection->section = $section_num;
        $thissection->name = null;
        $thissection->summary = '';
        $thissection->summaryformat = FORMAT_HTML;
        $thissection->visible = 1;
        $thissection->id = $DB->insert_record('course_sections', $thissection);

        // Only needed for 2.3 — GJN
        $thissection->uservisible = true;
        $thissection->availableinfo = null;
        $thissection->showavailability = 0;

        $sections[$section_num] = $thissection;
    }
    return $thissection;
}

/**
 * Determine whether a section is visible to the current user.
 * @param object $thissection The section (as a data structure, not an id)
 * @param boolean $canviewhidden Whether the user can view hidden sections
 * @return boolean True if the section is visible
 */
function format_tiles_is_section_user_visible($thissection, $canviewhidden) {
    // Needs to work with both 2.2 and 2.3, however the way course formats has
    // changed considerably in 2.3. A lot is precalculated and attached to the
    // section information that is passed to the format. I'm trying to use this
    // where possible and revert to old methods where it does not look like this
    // info is available.
    if (isset($thissection->uservisible)) {
        return $thissection->uservisible ||
                ($thissection->visible && !$thissection->available && $thissection->showavailability);
    } else {
        return $canviewhidden or $thissection->visible or !$course->hiddensections;
    }
}

/**
 * Get a name for a section (making it up if needed)
 * @param object $course The course (as an object)
 * @param object $section The section (as an object)
 * @return string a name for the section
 */
function format_tiles_get_section_name($course, $section) {
    // We can't add a node without any text
    if ((string) $section->name !== '') {
        return format_string($section->name, true, array('context' => context_course::instance($course->id)));
    } else if ($section->section == 0) {
        return get_string('section0name', 'format_tiles');
    } else {
        return get_string('sectionname', 'format_tiles') . ' ' . $section->section;
    }
}

/**
 * Display the contents of a section.
 * @global object $OUTPUT standard Moodle output class
 * @global object $PAGE standard Moodle page class
 * @param object $course The course
 * @param object $thissection The section
 * @param array $mods A list of available activity mods
 * @param array $modnames
 * @param array $modnamesused
 * @param boolean $canviewhidden Whether the user can view hidden sections
 */
function format_tiles_display_section($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden) {
    global $OUTPUT, $PAGE;
    $coursecontext = context_course::instance($course->id);

    $streditsummary = get_string('editsummary');

    if (!$canviewhidden and !$thissection->visible) {   // Hidden for students
        echo get_string('notavailable');
    } else {
        #if (!is_null($thissection->name)) {
        #    echo $OUTPUT->heading(format_string($thissection->name, true, array('context' => $coursecontext)), 3, 'sectionname');
        #}
        echo $OUTPUT->heading(format_string(format_tiles_get_section_name($course, $thissection), true, array('context' => $coursecontext)), 3, 'sectionname');

        echo html_writer::start_tag('div', array('class' => 'summary'));
        if ($thissection->summary) {
            $summarytext = file_rewrite_pluginfile_urls($thissection->summary, 'pluginfile.php', $coursecontext->id, 'course', 'section', $thissection->id);
            $summaryformatoptions = new stdClass();
            $summaryformatoptions->noclean = true;
            $summaryformatoptions->overflowdiv = true;
            echo format_text($summarytext, $thissection->summaryformat, $summaryformatoptions);
        } else {
            echo '&nbsp;';
        }

        if ($PAGE->user_is_editing() && has_capability('moodle/course:update', context_course::instance($course->id))) {
            echo html_writer::start_tag('a', array('href' => 'editsection.php?id=' . $thissection->id, 'title' => $streditsummary));
            echo html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/settings'), 'class' => 'iconsmall edit', 'alt' => $streditsummary));
            echo html_writer::end_tag('a');
            echo html_writer::empty_tag('br');
            echo html_writer::empty_tag('br');
        }
        echo html_writer::end_tag('div');

        $displayoptions = array('hidecompletion' => false);
        $courserenderer = $PAGE->get_renderer('core', 'course');
        echo $courserenderer->course_section_cm_list($course, $thissection, null, $displayoptions);
        echo html_writer::empty_tag('br');
        if ($PAGE->user_is_editing()) {
            echo $courserenderer->course_section_add_cm_control($course, $thissection->section, null,
                array('inblock' => false));
        }
    }
}

/**
 * Displays the nav bar that is used at the top of the single topic view
 * @param object $course The current course
 * @param array $sections The sections of the course
 * @param int $displaysection The section currently being displayed
 * @param boolean $canviewhidden Can the current user view hidden sections?
 */
function format_tiles_display_nav_bar($course, $sections, $displaysection, $canviewhidden, $nav_pos = 'tilenav_top') {
    $home_url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => 0));
    $home_title = get_string('topicoutline');

    static $formatconfig = false;
    if ($formatconfig === false) {
        $formatconfig = get_config('format_tiles');
    }

    echo html_writer::start_tag('div', array('class' => 'tiletopicnav ' . $nav_pos));
    echo html_writer::start_tag('ul');
    echo html_writer::start_tag('li');
    echo html_writer::start_tag('a', array('href' => $home_url, 'class' => 'summary', 'title' => $home_title));
    echo html_writer::tag('span', 'Home');
    echo html_writer::end_tag('a');
    echo html_writer::end_tag('li');
    echo "\n"; # Maintain compatability with existing style sheets

    $modinfo = get_fast_modinfo($course);

    foreach ($modinfo->get_section_info_all() as $section => $thissection) {

        // Don't show the home section in the nav bar
        if ($section == 0) continue;

        // Extra orphaned sections
        if ($section > $course->numsections) {
            continue;
        }

        $classes = array();
        $is_current_section = (!empty($displaysection) and $displaysection == $section);
        if ($is_current_section) {
            $classes[] = "current";
        }

        if (course_get_format($course)->is_section_current($section)) {
            $classes[] = "currentsection";
        }

        $button_class = "";
        $button_class = implode(' ', $classes);

        $showsection = format_tiles_is_section_user_visible($thissection, $canviewhidden);

        if ($showsection) {
            $url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => $section));
            $title = format_tiles_get_section_name($course, $thissection);
            $content = $section;
            if ($formatconfig->allow_title_tabs && $course->titletabs) {
                $content = $title;
                if ($formatconfig->max_title_length and strlen($content) > $formatconfig->max_title_length) {
                    $content = substr($content, 0, $formatconfig->max_title_length) . "...";
                }
            }
            echo html_writer::start_tag('li');
            echo html_writer::start_tag('a', array('href' => $url, 'class' => $button_class, 'title' => $title));
            echo html_writer::tag('span', $content);
            echo html_writer::end_tag('a');
            echo html_writer::end_tag('li');
            echo "\n"; # Maintain compatability with existing style sheets
        }

        $section++;
    }

    if ($formatconfig->allow_all_sections_view and $course->allsections) {
        $all_url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => 'all'));

        $button_class = 'allsections';
        if (!empty($displaysection) and $displaysection == 'all') {
            $button_class = 'current allsections';
        }

        echo html_writer::start_tag('li');
        echo html_writer::start_tag('a', array('href' => $all_url, 'class' => $button_class, 'title' => get_string('allsections', 'format_tiles')));
        echo html_writer::tag('span', get_string('allsections', 'format_tiles'));
        echo html_writer::end_tag('a');
        echo html_writer::end_tag('li');
        echo "\n"; # Maintain compatability with existing style sheets
    }

    echo html_writer::end_tag('ul');
    echo html_writer::end_tag('div'); #class="tiletopicnav"
}

/**
 * Display the content of a page in single section view.
 * @param object $course The current course
 * @param array $sections The sections of the course
 * @param int $displaysection The section currently being displayed
 * @param array $mods A list of available activity mods
 * @param array $modnames
 * @param array $modnamesused
 * @param boolean $canviewhidden Can the current user view hidden sections?
 */
function format_tiles_display_section_page($course, $sections, $displaysection, $mods, $modnames, $modnamesused, $canviewhidden) {

    static $formatconfig = false;
    if ($formatconfig === false) {
        $formatconfig = get_config('format_tiles');
    }

    format_tiles_display_nav_bar($course, $sections, $displaysection, $canviewhidden);

    if (!empty($displaysection)) {
        $thissection = format_tiles_get_or_create_section($course, $sections, $displaysection);
    }

    if (format_tiles_is_section_user_visible($thissection, $canviewhidden)) {
        echo html_writer::start_tag('div', array('class' => 'tilecontentarea'));
        format_tiles_display_section($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);
        echo html_writer::end_tag('div');
    }

    if ($formatconfig->allow_bottom_tabs and $course->bottomtabs) {
        format_tiles_display_nav_bar($course, $sections, $displaysection, $canviewhidden, 'tilenav_bottom');
    }
}

/**
 * Displays the course in all sections mode.
 * @param object $course The current course
 * @param array $sections The sections of the course
 * @param array $mods A list of available activity mods
 * @param array $modnames
 * @param array $modnamesused
 * @param boolean $canviewhidden Can the current user view hidden sections?
 */
function format_tiles_display_all_sections_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden) {
    global $OUTPUT;

    $modinfo = get_fast_modinfo($course);
    $numsections = count($modinfo);

    $section = 0;
    $thissection = $sections[$section];
    unset($sections[0]);

    format_tiles_display_nav_bar($course, $sections, 'all', $canviewhidden);

    // Frontpage outline area
    echo html_writer::start_tag('div', array('class' => 'tileoutlinearea'));
    echo $OUTPUT->heading(get_string('topicoutline'), 2, 'headingblock header outline');
    echo html_writer::start_tag('div', array('class' => 'tileoutlinecontent no-overflow'));
    format_tiles_display_section($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);
    echo html_writer::end_tag('div'); # class="tileoutlinecontent
    echo html_writer::end_tag('div'); #id="tileoutlinearea"

#    $strshowalltopics = get_string('showalltopics');

    // Output list of tiles
    echo html_writer::start_tag('ul', array('class' => 'tileoutlinetiles', 'id' => 'tileoutlinetiles'));
    $section = 1;

    echo html_writer::start_tag('div', array('class' => 'tilecontentarea'));
    echo html_writer::start_tag('ul', array('class' => 'tileothertopics'));
    foreach ($modinfo->get_section_info_all() as $section => $thissection) {

        // Don't show the home section as a tile
        if ($section == 0) continue;

        // Extra orphaned sections
        if ($section > $course->numsections) {
            continue;
        }

        $showsection = format_tiles_is_section_user_visible($thissection, $canviewhidden);

        if ($showsection) {

            $sectionclass = "";
            if (course_get_format($course)->is_section_current($section)) {
                $sectionclass = ' currentsection';
            }

            echo html_writer::start_tag('li', array('class' => 'tilecontentarea section main clearfix' . $sectionclass, 'id' => 'section-' . $section));
            format_tiles_display_section($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);
            echo html_writer::end_tag('li');
        }

        unset($sections[$section]);
    }
    echo html_writer::end_tag('ul');
    echo html_writer::end_tag('div');

    static $formatconfig = false;
    if ($formatconfig === false) {
        $formatconfig = get_config('format_tiles');
    }

    if ($formatconfig->allow_bottom_tabs and $course->bottomtabs) {
        format_tiles_display_nav_bar($course, $sections, 'all', $canviewhidden, 'tilenav_bottom');
    }

}

/**
 * Display a single tile
 * @param object $course The course (as an object)
 * @param int $section Current section number
 * @param object $thissection The section
 */
function format_tiles_display_tile($course, $section, $thissection) {
    global $DB, $OUTPUT;

    $tile_class = "";
    if (course_get_format($course)->is_section_current($section)) {
        $tile_class = ' currentsection';
    }

    $section_name = format_tiles_get_section_name($course, $thissection);
    $url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => $section));

    $coursecontext = context_course::instance($course->id);

    $bg_style = "";

    static $formatconfig = false;
    if ($formatconfig === false) {
        $formatconfig = get_config('format_tiles');
    }

    if ($formatconfig->allow_custom_tiles) {
        $tile_image = $DB->get_record('format_tiles_tile_image', array('courseid' => $course->id, 'sectionid' => $thissection->id));
        if ($tile_image) {

            $image_path = moodle_url::make_pluginfile_url(
                            $coursecontext->id, 'course', 'section', $thissection->id, '/format_tiles/',
                            $tile_image->filename);
            $bg_style = 'background-image: url(' . $image_path . ');';

        }
    }

    echo html_writer::start_tag('li', array('class' => 'tile' . $tile_class, 'id' => 'tile' . $section));
    if (!empty($bg_style)) {
        echo html_writer::start_tag('a', array('href' => $url, 'style' => $bg_style));
    } else {
        echo html_writer::start_tag('a', array('href' => $url));
    }
    echo html_writer::tag('span', $section_name, array('class' => 'tiletitle'));
    echo html_writer::tag('span', $section, array('class' => 'tiletopic'));
    echo html_writer::end_tag('a');
    echo html_writer::end_tag('li');
}

/**
 * Display the summary page for the course.
 * @global object $OUTPUT standard Moodle output class
 * @param object $course The course (as an object)
 * @param array $sections The sections (as objects) associated with the course.
 * @param array $mods A list of available activity mods
 * @param array $modnames
 * @param array $modnamesused
 * @param boolean $canviewhidden Whether the user can view hidden sections
 */
function format_tiles_display_course_summary_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden) {
    global $OUTPUT;

    $modinfo = get_fast_modinfo($course);
    $numsections = count($modinfo);

    $section = 0;
    $thissection = $sections[$section];
    unset($sections[0]);

    // Frontpage outline area
    echo html_writer::start_tag('div', array('class' => 'tileoutlinearea', 'id' => 'tileoutlinearea'));
    echo $OUTPUT->heading(get_string('topicoutline'), 2, 'headingblock header outline');
    echo html_writer::start_tag('div', array('class' => 'tileoutlinecontent no-overflow'));
    format_tiles_display_section($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);
    echo html_writer::end_tag('div'); # class="tileoutlinecontent
    echo html_writer::end_tag('div'); #id="tileoutlinearea"

#    $strshowalltopics = get_string('showalltopics');

    // Output list of tiles
    echo html_writer::start_tag('ul', array('class' => 'tileoutlinetiles', 'id' => 'tileoutlinetiles'));
    $section = 1;

    foreach ($modinfo->get_section_info_all() as $section => $thissection) {

        // Don't show the home section as a tile
        if ($section == 0) continue;

        // Extra orphaned sections
        if ($section > $course->numsections) {
            continue;
        }

        $showsection = format_tiles_is_section_user_visible($thissection, $canviewhidden);

        if ($showsection) {
            format_tiles_display_tile($course, $section, $thissection);
        }

        unset($sections[$section]);
        $section++;
    }

    static $formatconfig = false;
    if ($formatconfig === false) {
        $formatconfig = get_config('format_tiles');
    }

    if ($formatconfig->allow_all_sections_view and $course->allsections) {
        $all_url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => 'all'));

        $button_class = "";
        if (!empty($displaysection) and $displaysection == 'all') {
            $button_class = 'current';
        }

        echo html_writer::start_tag('li', array('class' => 'tile allsections', 'id' => 'tileall'));
        echo html_writer::start_tag('a', array('href' => $all_url));
        echo html_writer::tag('span', get_string('allsections', 'format_tiles'), array('class' => 'tiletitle'));
        echo html_writer::tag('span', '', array('class' => 'tiletopic'));
        echo html_writer::end_tag('a');
        echo html_writer::end_tag('li');
    }
    echo html_writer::end_tag('ul'); #id="tileoutlinearea"
}

/**
 * Display the course editting page.
 * @global object $OUTPUT standard Moodle output class
 * @global object $PAGE standard Moodle page class
 * @param object $course The course
 * @param array $mods A list of available activity mods
 * @param array $modnames
 * @param array $modnamesused
 * @param boolean $canviewhidden Whether the user can view hidden sections
 */
function format_tiles_display_course_editing_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden) {
    global $DB, $OUTPUT;

    # Strings used in edit controls.
    $strhidefromothers = get_string('hidefromothers', 'format_tiles');
    $strshowfromothers = get_string('showfromothers', 'format_tiles');
    $strmoveup = get_string('moveup');
    $strmovedown = get_string('movedown');

    $modinfo = get_fast_modinfo($course);
    $numsections = count($modinfo);

/// Now all the normal modules by topic
/// Everything below uses "section" terminology - each "section" is a topic.

    $formatconfig = get_config('format_tiles');

    echo html_writer::start_tag('ul', array('class' => 'tileothertopics'));

    foreach ($modinfo->get_section_info_all() as $section => $thissection) {

        // Extra orphaned sections
        if ($section > $course->numsections) {
            continue;
        }

        $showsection = format_tiles_is_section_user_visible($thissection, $canviewhidden);

        if (!empty($displaysection) and $displaysection != $section) {  // Check this topic is visible
            if ($showsection) {
                $sectionmenu[$section] = get_section_name($course, $thissection);
            }
            $section++;
            continue;
        }

        if ($showsection) {

            $currenttopic = ($course->marker == $section);

            if (!$thissection->visible) {
                $sectionstyle = ' hidden';
            } else if ($currenttopic) {
                $sectionstyle = ' current';
            } else {
                $sectionstyle = '';
            }

            echo html_writer::start_tag('li', array('class' => 'tilecontentarea section main clearfix' . $sectionstyle, 'id' => 'section-' . $section));
            echo html_writer::tag('div', $section, array('class' => 'left side'));

            // Note, 'right side' is BEFORE content.
            echo html_writer::start_tag('div', array('class' => 'right side'));

            $coursecontext = context_course::instance($course->id);

            if (has_capability('moodle/course:sectionvisibility', $coursecontext)) {
                if ($thissection->visible) {        // Show the hide/show eye
                    echo html_writer::start_tag('a', array('href' => 'view.php?id=' . $course->id . '&amp;hide=' . $section . '&amp;sesskey=' . sesskey() . '#section-' . $section, 'title' => $strhidefromothers));
                    echo html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/hide'), 'class' => 'icon hide', 'alt' => $strhidefromothers));
                    echo html_writer::end_tag('a');
                    echo html_writer::empty_tag('br');
                } else {
                    echo html_writer::start_tag('a', array('href' => 'view.php?id=' . $course->id . '&amp;show=' . $section . '&amp;sesskey=' . sesskey() . '#section-' . $section, 'title' => $strshowfromothers));
                    echo html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/show'), 'class' => 'icon hide', 'alt' => $strshowfromothers));
                    echo html_writer::end_tag('a');
                    echo html_writer::empty_tag('br');
                }
            }
            if (has_capability('moodle/course:update', $coursecontext)) {
                if ($section > 1) {                       // Add a arrow to move section up
                    echo html_writer::start_tag('a', array('href' => 'view.php?id=' . $course->id . '&amp;random=' . rand(1, 10000) . '&amp;section=' . $section . '&amp;move=-1&amp;sesskey=' . sesskey() . '#section-' . ($section - 1), 'title' => $strmoveup));
                    echo html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/up'), 'class' => 'icon up', 'alt' => $strmoveup));
                    echo html_writer::end_tag('a');
                    echo html_writer::empty_tag('br');
                }
                if ($section != 0 && $section < $course->numsections) {    // Add a arrow to move section down
                    echo html_writer::start_tag('a', array('href' => 'view.php?id=' . $course->id . '&amp;random=' . rand(1, 10000) . '&amp;section=' . $section . '&amp;move=1&amp;sesskey=' . sesskey() . '#section-' . ($section + 1), 'title' => $strmovedown));
                    echo html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/down'), 'class' => 'icon down', 'alt' => $strmovedown));
                    echo html_writer::end_tag('a');
                    echo html_writer::empty_tag('br');
                }
            }

            $url = new moodle_url('/course/view.php', array('id' => $course->id));
            $url->param('sesskey', sesskey());

            if (has_capability('moodle/course:setcurrentsection', $coursecontext)) {
                if ($course->marker == $thissection->section) {  // Show the "light globe" on/off.
                    $url->param('marker', 0);
                    echo html_writer::link($url,
                                        html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/marked'),
                                            'class' => 'icon ', 'alt' => get_string('markedthistopic'))),
                                        array('title' => get_string('markedthistopic'), 'class' => 'editing_highlight'));
                } else {
                    $url->param('marker', $thissection->section);
                    echo html_writer::link($url,
                                    html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/marker'),
                                        'class' => 'icon', 'alt' => get_string('markthistopic'))),
                                    array('title' => get_string('markthistopic'), 'class' => 'editing_highlight'));
                }
            }
            echo html_writer::end_tag('div'); //class="right side"

            echo html_writer::start_tag('div', array('class' => 'content'));
            format_tiles_display_section($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);

            if ($formatconfig->allow_custom_tiles && $section > 0 && has_capability('format/tiles:changetilebackground', $coursecontext)) {
                echo html_writer::empty_tag('hr');
                echo html_writer::tag('h4', get_string('tile_preview', 'format_tiles'));

                echo html_writer::start_tag('ul', array('class' => 'tileoutlinetiles', 'id' => 'tileoutlinetiles'));

                format_tiles_display_tile($course, $section, $thissection);
                #TODO: cache the following as it's also retreived in the previous function.
                $tile_image = $DB->get_record('format_tiles_tile_image', array('courseid' => $course->id, 'sectionid' => $thissection->id));

                echo html_writer::start_tag('li', array('class' => 'tilenotes'));
                if ($tile_image) {
                    $user = $DB->get_record('user', array('id' => $tile_image->userid));
                    $date = date('Y-m-d', $tile_image->timemodified);
                    echo get_string('background_set', 'format_tiles', array('firstname' => $user->firstname, 'lastname' => $user->lastname, 'date' => $date));
                    echo html_writer::empty_tag('br');
                    echo html_writer::link(new moodle_url('/course/format/tiles/chooseimage.php', array('courseid' => $course->id, 'sectionid' => $thissection->id)), get_string('change_background', 'format_tiles'));
                    echo html_writer::empty_tag('br');
                    echo html_writer::link(new moodle_url('/course/format/tiles/removeimage.php', array('courseid' => $course->id, 'sectionid' => $thissection->id)), get_string('remove_background', 'format_tiles'));
                } else {
                    echo get_string('background_not_set', 'format_tiles');
                    echo html_writer::empty_tag('br');
                    echo html_writer::link(new moodle_url('/course/format/tiles/chooseimage.php', array('courseid' => $course->id, 'sectionid' => $thissection->id)), get_string('set_background', 'format_tiles'));
                }
                echo html_writer::end_tag('li');
                echo html_writer::end_tag('ul'); #id="tileoutlinearea"
            }

            echo html_writer::end_tag('div');
            echo html_writer::end_tag('li');
        }

        unset($sections[$section]);
        $section++;
    }

    if (has_capability('moodle/course:update', context_course::instance($course->id))) {
        // print stealth sections if present
        $modinfo = get_fast_modinfo($course);
        foreach ($sections as $section => $thissection) {
            if (empty($modinfo->sections[$section])) {
                continue;
            }

            echo html_writer::start_tag('li', array('class' => 'section main clearfix orphaned hidden', 'id' => 'section-' . $section));
            echo html_writer::tag('div', '', array('class' => 'left side'));
            echo html_writer::tag('div', '', array('class' => 'right side'));
            echo html_writer::start_tag('div', array('class' => 'content'));
            echo $OUTPUT->heading(get_string('orphanedactivities'), 3, 'sectionname');
            print_section($course, $thissection, $mods, $modnamesused);
            echo html_writer::end_tag('div');
            echo html_writer::end_tag('li');
        }
    }

    echo html_writer::end_tag('ul');
}
?>