<?php

/** 
 * Androgogic Support Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class support_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$mform->addElement('html','<div>');
//inputs 
$mform->addElement('text','firstname',get_string('firstname'));
$mform->addElement('text','lastname',get_string('lastname'));
$mform->addElement('text','email',get_string('email'));
$mform->addElement('text','phone',get_string('phone'));
$mform->addElement('text','description',get_string('description'));

//button
$mform->addElement('submit','submit','Submit');
$mform->addElement('html','</div>');
}
}
