<?php

/** 
 * Androgogic Support Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class faq_edit_form extends moodleform {
protected $faq;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = get_context_instance(CONTEXT_SYSTEM);
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.*  
from mdl_androgogic_faq a 
where a.id = {$_REQUEST['id']} ";
$faq = $DB->get_record_sql($q);
}
else{
$faq = $this->_customdata['$faq']; // this contains the data of this form
}
$tab = 'faq_new'; // from whence we were called
if (!empty($faq->id)) {
$tab = 'faq_edit';
}
$mform->addElement('html','<div>');

//question
$editoroptions = array('maxfiles' => 0, 'maxbytes' => 0, 'trusttext' => false, 'forcehttps' => false);
$mform->addElement('editor', 'question', get_string('question','block_androgogic_support'), null, $editoroptions);
$mform->setType('question', PARAM_CLEANHTML);
$mform->addRule('question', get_string('required'), 'required', null, 'server');

//answer
$editoroptions = array('maxfiles' => 0, 'maxbytes' => 0, 'trusttext' => false, 'forcehttps' => false);
$mform->addElement('editor', 'answer', get_string('answer','block_androgogic_support'), null, $editoroptions);
$mform->setType('answer', PARAM_CLEANHTML);
$mform->addRule('answer', get_string('required'), 'required', null, 'server');

//listing_order
$mform->addElement('text', 'listing_order', get_string('listing_order','block_androgogic_support'), array('size'=>5));
$mform->addRule('listing_order', get_string('required'), 'required', null, 'server');
$mform->setType('listing_order', PARAM_INT);

//set values if we are in edit mode
if (!empty($faq->id) && isset($_GET['id'])) {
$question['text'] = $faq->question;
$question['format'] = 1;
$mform->setDefault('question', $question);
$answer['text'] = $faq->answer;
$answer['format'] = 1;
$mform->setDefault('answer', $answer);
$mform->setConstant('listing_order', $faq->listing_order);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons();
$mform->addElement('html','</div>');
}
}
