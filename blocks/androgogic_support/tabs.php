<?php
/** 
 * Androgogic Support Block: Tabs
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

// This file to be included so we can assume config.php has already been included.
if (empty($currenttab)) {
error('You cannot call this script in that way');
}
$tabs = array();
$row = array();
if(has_capability('block/androgogic_support:edit', $context)){
    $row[] = new tabobject('server_status_admin', $CFG->wwwroot.'/blocks/androgogic_support/index.php?tab=server_status_search', get_string('server_status_search','block_androgogic_support'));
        
}
if(is_siteadmin()){
    $row[] = new tabobject('support_log', $CFG->wwwroot.'/blocks/androgogic_support/index.php?tab=support_log_search', get_string('support_log_search','block_androgogic_support'));
}
$row[] = new tabobject('server_status', $CFG->wwwroot.'/blocks/androgogic_support/index.php?tab=server_status', get_string('support_log_new','block_androgogic_support'));
$row[] = new tabobject('faq', $CFG->wwwroot.'/blocks/androgogic_support/index.php?tab=faq_search', get_string('faq_search','block_androgogic_support'));
$tabs[] = $row;
// Print out the tabs and continue!
print_tabs($tabs, $currenttab);

// End of blocks/androgogic_support/tabs.php