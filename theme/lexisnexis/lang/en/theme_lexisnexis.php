<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_lexisnexis', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   theme_lexisnexis
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Lexis Nexis';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['choosereadme'] = '<div class="clearfix"><div class="theme_screenshot"><h2>Lexis Nexis</h2><img src="lexisnexis/pix/screenshot.jpg" /><h3>Theme Credits</h3><p>Created by Androgogic</p><h3>Report a bug:</h3><p><a href="https://androgogic.livetime.com/">https://androgogic.livetime.com/</a></p></div><div class="theme_description"><h2>About</h2><p>Lexis Nexis is a fluid-width, three-column Moodle 2.0 theme with rounded corners. <h2>Tweaks</h2><p>This theme is built upon the Anomaly and Base themes inside the Moodle core.</p><h2>Credits</h2><p>This theme was originally designed for Moodle 1.9 by Patrick Malley. It was then coded for 2.0 and is maintained by Sam Hemelryk at Moodle HQ. The Lexis Nexis customisation was performed by Androgogic Pty Ltd for Lexis Nexis. </p><h2>License</h2><p>This, and all other themes included in the Moodle core, are licensed under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.</div></div>';