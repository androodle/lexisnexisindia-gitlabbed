<?php

$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$showsidepre = $hassidepre && !$PAGE->blocks->region_completely_docked('side-pre', $OUTPUT);
$showsidepost = $hassidepost && !$PAGE->blocks->region_completely_docked('side-post', $OUTPUT);
$custommenu = $OUTPUT->custom_menu();
$hascustommenu = (empty($PAGE->layout_options['nocustommenu']) && !empty($custommenu));

$bodyclasses = array();
if ($showsidepre && !$showsidepost) {
    $bodyclasses[] = 'side-pre-only';
} else if ($showsidepost && !$showsidepre) {
    $bodyclasses[] = 'side-post-only';
} else if (!$showsidepost && !$showsidepre) {
    $bodyclasses[] = 'content-only';
}
if ($hascustommenu) {
    $bodyclasses[] = 'has_custom_menu';
}
if ($hasnavbar) {
    $bodyclasses[] = 'hasnavbar';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
    <title><?php echo $PAGE->title ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses.' '.join(' ', $bodyclasses)) ?>">
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<div id="page">
<?php if ($hasheading || $hasnavbar) { ?>
    <div id="page-header">
        <?php if ($hasheading) { ?>
              <div id="lexisnexislogo"><a href="<?php echo $CFG->httpswwwroot; ?>"><img alt="Logo linking to Lexis Nexis home" src="<?php echo $OUTPUT->pix_url('logo', 'theme')?>" /></a></div>
        <div class="headermenu">
<?php
            echo $OUTPUT->login_info();
            if(!empty($USER->id)){?>
         <span id="lexisphoto">
         <?php 
         echo $OUTPUT->user_picture($USER, array('size'=>48));?></span> 
         <?php }
          if (!empty($PAGE->layout_options['langmenu'])) {
                echo $OUTPUT->lang_menu();
            }
            echo $PAGE->headingmenu
        ?></div><?php } ?>

        <?php if ($hascustommenu) { ?>
 	<span id="custommenu"><?php echo $custommenu; ?></span>
		<?php } ?>

            <div class="navbar clearfix">
                <div class="breadcrumb"><?php echo $OUTPUT->navbar(); ?></div>
                <div class="navbutton"><?php echo $PAGE->button; ?></div>
            </div>
</div>

<?php } ?>
<!-- END OF HEADER -->

    <div id="page-content">
        <div id="region-main-box">
            <div id="region-post-box">

                <div id="region-main-wrap">
                    <div id="region-main">
                        <div class="region-content">
                            <?php echo $OUTPUT->main_content() ?>
                        </div>
                    </div>
                </div>

                <?php if ($hassidepre) { ?>
                <div id="region-pre" class="block-region">
                    <div class="region-content">
                        <?php echo $OUTPUT->blocks_for_region('side-pre') ?>
                    </div>
                </div>
                <?php } ?>

                <?php if ($hassidepost) { ?>
                <div id="region-post" class="block-region">
                    <div class="region-content">
                        <?php echo $OUTPUT->blocks_for_region('side-post') ?>
                    </div>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>

<!-- START OF FOOTER -->
<?php if ($hasfooter) { ?>
<div id="footerwrap">
    <div id="page-footer" class="clearfix">
        <div class="footer">
            <a href="http://www.lexisnexis.com.au"><img src="<?php echo $OUTPUT->pix_url('lexisnexis', 'theme')?>" /></a>
            <span>
                <a href="<?php echo $CFG->httpswwwroot.'/theme/lexisnexis/pix/lexisnexiscampus_termsofuse.pdf' ?>">Terms and Conditions</a> &nbsp; | &nbsp; <a href="http://www.lexisnexis.com.au/en-au/privacy-statement.page">Privacy Information</a> &nbsp; | &nbsp; <a href="http://www.lexisnexis.com.au/terms/copyright.aspx">Copyright &copy; 2014 LexisNexis</a>. All Rights reserved.
            </span>
            <div class="social">
                <a href="http://www.facebook.com/lexisnexisindiapage"><img src="<?php echo $OUTPUT->pix_url('footer_facebook', 'theme')?>" /></a>
                <a href="https://www.linkedin.com/company/lexisnexis-india"><img src="<?php echo $OUTPUT->pix_url('footer_linkedin', 'theme')?>" /></a>
                <a href="http://www.twitter.com/lexisnexisindia"><img src="<?php echo $OUTPUT->pix_url('footer_twitter', 'theme')?>" /></a>
                <a href="http://www.lexisnexisindia.blogspot.in"><img src="<?php echo $OUTPUT->pix_url('footer_blogger', 'theme')?>" /></a>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="clearfix"></div>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>