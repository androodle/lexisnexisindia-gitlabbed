<?php
    /**
     * Theme version info
     *
     * @package    theme
     * @subpackage androtheme
     */

	 //Documentation: http://docs.moodle.org/dev/version.php
	 
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014062300; // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2013050100; // Requires this Moodle version
$plugin->component = 'theme_androtheme'; // Type and name of this plugin
$plugin->maturity = MATURITY_STABLE; // This is considered as ready for production sites
$plugin->release = 'v1.1'; //Version of this plugin
$plugin->dependencies = array(
	'theme_bootstrapbase'  => 2013050100,
);