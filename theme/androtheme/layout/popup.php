<?php

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = !empty($custommenu);

$haslogininfo = (empty($PAGE->layout_options['nologininfo']));
$showmenu = empty($PAGE->layout_options['nocustommenu']);

if ($showmenu && !$hascustommenu) {
    // load totara menu
    $menudata = totara_build_menu();
    $totara_core_renderer = $PAGE->get_renderer('totara_core');
    $totaramenu = $totara_core_renderer->print_totara_menu($menudata);
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<?php
// If on desktop, then hide the header/footer.
$hideclass = '';
$bodynoheader = '';
$devicetype = get_device_type();
if ($devicetype !== 'mobile' and $devicetype !== 'tablet') {
    // We can not use the Bootstrap responsive css classes because popups are phone sized on desktop.
    $hideclass = 'hide';
    $bodynoheader = 'bodynoheader';
}
?>

<body <?php echo $OUTPUT->body_attributes(array($bodynoheader)); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<header role="banner" class="navbar navbar-fixed-top moodle-has-zindex <?php echo $hideclass; ?>">
    <nav role="navigation" class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="<?php echo $CFG->wwwroot;?>"><?php echo $SITE->shortname; ?></a>
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse collapse">
                <?php if ($showmenu) { ?>
                    <?php if ($hascustommenu) { ?>
                    <div id="custommenu"><?php echo $custommenu; ?></div>
                    <?php } else { ?>
                    <div id="totaramenu"><?php echo $totaramenu; ?></div>
                    <?php } ?>
                <?php } ?>
                <ul class="nav pull-right">
                    <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                    <li class="navbar-text"><?php echo $OUTPUT->login_info() ?></li>
					<li class="user-picture"><?php if (isloggedin()) {echo $OUTPUT->user_picture($USER, array('size'=>20));}?></li>
                </ul>
            </div>
        </div>
    </nav>
	<div id="page-header" class="clearfix">
		<div id="page-header-wrapper" class="container-fluid">
			<div class="logo">
				<a href="<?php echo $CFG->wwwroot; ?>"> <img src="<?php echo $logourl; ?>" alt="<?php print_string('home'); ?>" title="<?php print_string('home'); ?>"/></a>
			</div>
			<?php echo $html->bannertext; ?>
		</div>
		
		<div id="page-navbar-wrapper" class="clearfix">	
			<div id="page-navbar" class="container-fluid">
				<nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
				<div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
			</div>
			<!--<?php echo $OUTPUT->page_heading(); ?>
			<div id="course-header">
				<?php echo $OUTPUT->course_header(); ?>
			</div>-->
		</div>
	</div>
</header>

<div id="page" class="container-fluid">
	<div id="page-content" class="row-fluid">
        <section id="region-main" class="span12">
            <?php
            echo $OUTPUT->course_content_header();
            echo $OUTPUT->main_content();
            echo $OUTPUT->course_content_footer();
            ?>
        </section>
    </div>
</div>

<footer id="page-footer" class="<?php echo $hideclass; ?>">
	<div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
	<p class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></p>
	<?php
	echo $OUTPUT->login_info();
	echo $OUTPUT->home_link();
	echo $OUTPUT->standard_footer_html();
	?>
</footer>

<?php echo $OUTPUT->standard_end_of_body_html() ?>

</body>
</html>
