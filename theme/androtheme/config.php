<?php

$THEME->name = 'androtheme';
$THEME->doctype = 'html5';
$THEME->parents = array('bootstrapbase', 'standardtotararesponsive');
$THEME->sheets = array(
	'totara',
	'moodle', 
	'scorm', 
	'tiles', 
	'responsive', 
	'navigation', 
	'effects'
);
$THEME->supportscssoptimisation = false;
$THEME->yuicssmodules = array();

$THEME->editor_sheets = array();

$THEME->plugins_exclude_sheets = array(
    /*
	'block' => array('html',),
    'gradereport' => array('grader',),
	'mod' => array('scorm'),
	*/
);


$THEME->layouts = array(
    'admin' => array(
        'file' => 'columns3.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-post',
    ),
	// The site home page.
    'frontpage' => array(
        'file' => 'columns3.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-post',
    ),
	// My dashboard page
    'mydashboard' => array(
        'file' => 'columns3.php',
        'regions' => array('side-pre', 'content', 'side-post'),
        'defaultregion' => 'side-pre',
        'options' => array('langmenu'=>true),
    ),
    'course' => array(
        'file' => 'columns3.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-post',
    ),
    'incourse' => array(
        'file' => 'columns3.php',
        'regions' => array('side-pre', 'side-post'),
        //'defaultregion' => 'side-post',
		'defaultregion' => 'side-pre',
    ),
    'coursecategory' => array(
        'file' => 'columns3.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-post',
    ),
	// Pages that appear in pop-up windows - no navigation, no blocks, no header.
    'popup' => array(
        'file' => 'popup.php',
        'regions' => array(),
        'options' => array('noheader'=>true, 'nofooter'=>true, 'nonavbar'=>true),
    ),
	'noblocks' => array(
        'file' => 'columns1.php',
        'regions' => array(),
        'options' => array('noblocks'=>true, 'langmenu'=>true),
    ),
	'login' => array(
        'file' => 'columns1.php',
        'regions' => array(),
        'options' => array('nologininfo' => true, 'nocustommenu' => true, 'nonavbar' => false),
    ),
	// hide the login info section during maintenance
    'maintenance' => array(
        'file' => 'columns1.php',
        'regions' => array(),
        'options' => array('noblocks' => true, 'nofooter' => true, 'nonavbar' => true, 'nocustommenu' => true, 'nologininfo' => true, 'langmenu' => false, 'nocourseheaderfooter' => true),
    )
);


// special class tells Moodle to look for renderers first within the theme and then in all of the other default locations.
$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->csspostprocess = 'theme_androtheme_process_css';

$THEME->blockrtlmanipulations = array(
    'side-pre' => 'side-post',
    'side-post' => 'side-pre'
);

$THEME->enable_dock = true;